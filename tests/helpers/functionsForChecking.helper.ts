import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}

export function checkErrorMessage(
    responseErrorMessage,
    errorMessage:
        | "'Email' is not a valid email address."
        | "Password must be from 4 to 16 characters."
        | `Entity User with id (${number}) was not found.`
) {
    expect(responseErrorMessage, `ERROR: "${errorMessage}"`).to.be.equal(errorMessage);
}

export function checkUserInfo(responseInfo, expectedInfo) {
    expect(responseInfo.avatar, `Avatar should be ${expectedInfo.avatar}"`).to.be.equal(expectedInfo.avatar);
    expect(responseInfo.email, `Email should be ${expectedInfo.email}"`).to.be.equal(expectedInfo.email);
    expect(responseInfo.userName, `UserName should be ${expectedInfo.userName}"`).to.be.equal(expectedInfo.userName);
}

export function checkCreatedPost(responseInfo, author, previewImage, body) {
    expect(responseInfo.author, `Author should be ${author}"`).to.be.deep.equal(author);
    expect(responseInfo.previewImage, `Preview image should be ${previewImage}"`).to.be.equal(previewImage);
    expect(responseInfo.body, `Body should be ${body}"`).to.be.equal(body);
}

export function checkLikeReaction(responseInfo, postId, userId) {
    expect(responseInfo.id, `The post ID should be ${postId}"`).to.be.equal(postId);
    expect(responseInfo.reactions[0].isLike, `The "isLike" value should be ${true}"`).to.be.equal(true);
    expect(responseInfo.reactions[0].user.id, `The user ID should be ${userId}"`).to.be.equal(userId);
}
