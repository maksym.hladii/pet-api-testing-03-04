import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import {
    checkStatusCode,
    checkResponseTime,
    checkErrorMessage,
    checkUserInfo,
} from "../../helpers/functionsForChecking.helper";

const authController = new AuthController();
const registerController = new RegisterController();
const usersController = new UsersController();

const authSchemas = require("./data/auth_schemas_testData.json");
const registerSchemas = require("./data/register_schemas_testData.json");
const usersSchemas = require("./data/users_schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Users controller", () => {
    let invalidEmailUserData = [
        { id: 0, avatar: "http://example.com/Clint.png", email: "clint.barton@gmail@com", userName: "Hawkeye", password: "S.H.I.E.L.D.", },
        { id: 0, avatar: "http://example.com/Natasha.png", email: "natasha.romanoff.gmail.com", userName: "BlackWidow", password: "S.H.I.E.L.D.", },
    ];

    let invalidPasswordUserData = [
        { id: 0, avatar: "http://example.com/Thor.png", email: "thor.odinson@gmail.com", userName: "Thor", password: "123", },
        { id: 0, avatar: "http://example.com/Bruce.png", email: "bruce.banner@gmail.com", userName: "Hulk", password: "abcdefghij1234567", },
    ];

    let userDataToRegister = {
        id: 0,
        avatar: "http://example.com/Max.png",
        email: "maksym.hladiy@gmail.com",
        userName: "Max",
        password: "BinaryStudio2024",
    };
    let userDataRegistered, userDataUpdated;
    let accessToken: string;

    invalidEmailUserData.forEach((credentials) => {
        it(`Should return 400 error code when trying to register with an invalid email: '${credentials.email}'`, async () => {
            let response = await registerController.register(credentials);

            checkStatusCode(response, 400);
            checkResponseTime(response, 3000);
            checkErrorMessage(response.body.errors[0], "'Email' is not a valid email address.");
        });
    });

    invalidPasswordUserData.forEach((credentials) => {
        it(`Should return 400 error code when trying to register with an invalid password: '${credentials.password}'`, async () => {
            let response = await registerController.register(credentials);

            checkStatusCode(response, 400);
            checkResponseTime(response, 3000);
            checkErrorMessage(response.body.errors[0], "Password must be from 4 to 16 characters.");
        });
    });

    it(`Should return 201 status code after the user's successful registration`, async () => {
        let response = await registerController.register(userDataToRegister);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(registerSchemas.schema_register);
        checkUserInfo(response.body.user, userDataToRegister);

        userDataRegistered = {
            id: response.body.user.id,
            avatar: response.body.user.avatar,
            email: response.body.user.email,
            userName: response.body.user.userName,
        };
        accessToken = response.body.token.accessToken.token;
    });

    it(`Should return 200 status code and the list of all users`, async () => {
        let response = await usersController.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        expect(response.body).to.be.jsonSchema(usersSchemas.schema_allUsers);
        checkUserInfo(response.body[response.body.length - 1], userDataRegistered);
    });

    it(`Should return 200 status code after authorizing with the registered credentials`, async () => {
        let response = await authController.login(userDataRegistered.email, userDataToRegister.password);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(authSchemas.schema_login);
        checkUserInfo(response.body.user, userDataRegistered);

        accessToken = response.body.token.accessToken.token;
    });

    it(`Should return 200 status code and the current user's information by token`, async () => {
        let response = await usersController.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(usersSchemas.schema_user);
        checkUserInfo(response.body, userDataRegistered);
    });

    it(`Should return 204 status code and update the user's username using valid data`, async () => {
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataUpdated = {
            id: userDataRegistered.id,
            avatar: userDataRegistered.avatar,
            email: userDataRegistered.email,
            userName: replaceLastThreeWithRandom(userDataRegistered.userName),
        };

        let response = await usersController.updateUser(userDataUpdated, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`Should return 200 status code and correct user's information by token after updating the username`, async () => {
        let response = await usersController.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(usersSchemas.schema_user);
        checkUserInfo(response.body, userDataUpdated);
    });

    it(`Should return 200 status code and correct user's information by ID after updating the username`, async () => {
        let response = await usersController.getUserById(userDataUpdated.id);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(usersSchemas.schema_user);
        checkUserInfo(response.body, userDataUpdated);
    });

    it(`Should return 204 status code after successfully deleting the user by ID`, async () => {
        let response = await usersController.deleteUserById(userDataUpdated.id, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it(`Should return 404 error code when trying to update a non-existing user`, async () => {
        let invalidUserData = {
            id: userDataUpdated.id,
            avatar: "http://example.com/Max.jpg",
            email: "maksym.hladiy@gmail.com",
            userName: "Max",
        };

        let response = await usersController.updateUser(invalidUserData, accessToken);

        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);
        checkErrorMessage(response.body.error, `Entity User with id (${invalidUserData.id}) was not found.`);
    });
});
