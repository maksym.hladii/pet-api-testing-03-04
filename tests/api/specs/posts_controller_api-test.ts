import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import {
    checkStatusCode,
    checkResponseTime,
    checkUserInfo,
    checkCreatedPost,
    checkLikeReaction,
} from "../../helpers/functionsForChecking.helper";

const authController = new AuthController();
const registerController = new RegisterController();
const usersController = new UsersController();
const postsController = new PostsController();

const authSchemas = require("./data/auth_schemas_testData.json");
const registerSchemas = require("./data/register_schemas_testData.json");
const postsSchemas = require("./data/posts_schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Posts controller", () => {
    let user1DataToRegister = {
        id: 0,
        avatar: "http://example.com/Steve.png",
        email: "steven.rogers@gmail.com",
        userName: "CaptainAmerica",
        password: "BinaryStudio2024",
    };
    let user2DataToRegister = {
        id: 0,
        avatar: "http://example.com/Tony.png",
        email: "anthony.stark@gmail.com",
        userName: "IronMan",
        password: "BinaryStudio2024",
    };
    let user1DataRegistered, user2DataRegistered;
    let user1AccessToken, user2AccessToken;
    let postDataToRegister, postDataRegistered;

    before(`Register two users, save the credentials and access tokens`, async () => {
        let response1 = await registerController.register(user1DataToRegister);
        checkStatusCode(response1, 201);
        expect(response1.body).to.be.jsonSchema(registerSchemas.schema_register);
        checkUserInfo(response1.body.user, user1DataToRegister);

        user1DataRegistered = {
            id: response1.body.user.id,
            avatar: response1.body.user.avatar,
            email: response1.body.user.email,
            userName: response1.body.user.userName,
        };
        user1AccessToken = response1.body.token.accessToken.token;

        let response2 = await registerController.register(user2DataToRegister);
        checkStatusCode(response2, 201);
        expect(response2.body).to.be.jsonSchema(registerSchemas.schema_register);
        checkUserInfo(response2.body.user, user2DataToRegister);

        user2DataRegistered = {
            id: response2.body.user.id,
            avatar: response2.body.user.avatar,
            email: response2.body.user.email,
            userName: response2.body.user.userName,
        };
        user2AccessToken = response2.body.token.accessToken.token;
    });

    it(`Should return 200 status code after authorizing with the first registered credentials`, async () => {
        let response = await authController.login(user1DataRegistered.email, user1DataToRegister.password);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(authSchemas.schema_login);
        checkUserInfo(response.body.user, user1DataRegistered);

        user1AccessToken = response.body.token.accessToken.token;
    });

    it(`Should return 200 status code after successfully creating a post`, async () => {
        postDataToRegister = {
            authorId: user1DataRegistered.id,
            previewImage: "previewImage.png",
            body: "body",
        };

        let response = await postsController.createPost(postDataToRegister, user1AccessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(postsSchemas.schema_post);
        checkCreatedPost(
            response.body,
            user1DataRegistered,
            postDataToRegister.previewImage,
            postDataToRegister.body
        );

        postDataRegistered = {
            postId: response.body.id,
            previewImage: response.body.previewImage,
            body: response.body.body,
        };
    });

    it(`Should return 200 status code and the list of all posts, and to check if the post is created`, async () => {
        let response = await postsController.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        expect(response.body).to.be.jsonSchema(postsSchemas.schema_allPosts);
        checkCreatedPost(
            response.body[0],
            user1DataRegistered,
            postDataRegistered.previewImage,
            postDataRegistered.body
        );
    });

    it(`Should return 200 status code after authorizing with the second registered credentials`, async () => {
        let response = await authController.login(user2DataRegistered.email, user2DataToRegister.password);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(authSchemas.schema_login);
        checkUserInfo(response.body.user, user2DataRegistered);

        user2AccessToken = response.body.token.accessToken.token;
    });

    it(`Should return 200 status code after successfully liking the post`, async () => {
        let response = await postsController.likePost(
            postDataRegistered.postId,
            user2DataRegistered.id,
            user2AccessToken
        );

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it(`Should return 200 status code and the list of all posts, and to check if the post is liked`, async () => {
        let response = await postsController.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        expect(response.body).to.be.jsonSchema(postsSchemas.schema_allPosts);
        checkLikeReaction(response.body[0], postDataRegistered.postId, user2DataRegistered.id);
    });

    it(`Should return 500 internal server error when trying to like a non-existing post`, async () => {
        let response = await postsController.likePost(0, user2DataRegistered.id, user2AccessToken);

        checkStatusCode(response, 500);
        checkResponseTime(response, 1000);
    });

    after(`Should return 204 status code after successfully deleting created users`, async () => {
        let user2Response = await usersController.deleteUserById(user2DataRegistered.id, user2AccessToken);
        checkStatusCode(user2Response, 204);

        let user1Response = await usersController.deleteUserById(user1DataRegistered.id, user1AccessToken);
        checkStatusCode(user1Response, 204);
    });
});
