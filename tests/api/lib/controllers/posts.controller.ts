import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async likePost(postIdValue, userIdValue, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: postIdValue,
                isLike: true,
                userId: userIdValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
